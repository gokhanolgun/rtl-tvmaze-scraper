# TvMaze Scraper and API for RTL

API layer for TvMaze

## Installation

Make sure you have [Docker](http://docker.com/) installed.

```sh
$ docker-compose build
```

## Starting server

```sh
$ docker-compose up
```

App will be running on [localhost:3000](http://localhost:3000/).

You can change settings in `.env` file.

## Running Tests

```sh
$ npm test
```

## Operating logic and explanations

A scraper starts to get show information from shows index page by page asynchronously when application starts to be able to start serving and to avoid TV Maze API's rate limit. After scraping basic show information, it starts to update all shows with casts information. In the meantime if client requests a page with no casts information, casts information for that page fetched on demand and updated to database. Also there is a cron working to update show information every hour by checking TV Maze's update information.

## API end-points

You can find **SWAGGER** API documentation in [localhost:3000/api-docs](http://localhost:3000/api-docs).

### /shows?page=%d

Request method: **GET**

#### Get all shows

- URL: [localhost:3000/shows?page=1](http://localhost:3000/shows?page=1)

##### Example response

```json
[
  {
    "id": 1,
    "name": "Show's Name",
    "casts": [
      {
        "id": 1,
        "name": "Person's Name",
        "birthday": "1970-01-01"
      }
    ]
  }
]
```