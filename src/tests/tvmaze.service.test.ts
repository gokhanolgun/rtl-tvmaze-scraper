import * as dotenv from 'dotenv';
import TvmazeService from '../services/tvmaze.service';

dotenv.config();

describe('TvmazeService', () => {
  const service = TvmazeService;

  it('should get show updates', async () => {
    const result = await service.getShowUpdates();
    const showCount = Object.keys(result).length;
    expect.assertions(1);
    await expect(showCount).toBeGreaterThan(0);
  });

  it('should get show cast info', async () => {
    const result = await service.getShowCasts(1);
    expect.assertions(1);
    await expect(result[0].person.id).toEqual(1);
  });

  it('should get show info with casts', async () => {
    const result = await service.getShowInfoWithCasts(1);
    expect.assertions(1);
    await expect(result.id).toEqual(1);
  });

  it('should get show index', async () => {
    const result = await service.getShowsIndex(0);
    expect.assertions(1);
    await expect(result[0].id).toEqual(1);
  });
});
