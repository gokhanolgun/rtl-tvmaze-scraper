interface Iembedded {
  cast?: Icast[];
}

export interface Iupdates {
  [key: number]: number;
}

export interface Icast {
  person: Iperson;
}

export interface Iperson {
  id: number;
  name: string;
  birthday: string;
}

export interface Ishow {
  id: number;
  name?: string;
  updated: number;
  _embedded?: Iembedded;
}
