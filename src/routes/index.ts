import {Request, Response} from 'express';
import * as swaggerUi from 'swagger-ui-express';
import * as swaggerDocument from '../config/swagger.config';
import {Database} from '../database/database';
import logger from '../helpers/logger.helper';
import * as ShowModel from '../models/show.model';
import {ShowService} from '../services/show.service';

const showService = new ShowService(new Database(ShowModel));

export class Routes {
  public routes(app): void {
    // Add access control headers and content type
    app.use((req, res, next) => {
      res.setHeader('Access-Control-Allow-Origin', '*');
      res.setHeader(
        'Access-Control-Allow-Methods',
        'GET, POST, OPTIONS, PUT, PATCH, DELETE'
      );
      res.setHeader(
        'Access-Control-Allow-Headers',
        'X-Requested-With,content-type'
      );
      res.setHeader('Access-Control-Allow-Credentials', 'true');
      next();
    });

    app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));

    app.route('/shows').get(async (req: Request, res: Response) => {
      let page = 1;
      if (req.query.page) {
        if (isNaN(Number(req.query.page))) {
          res.status(400).json({
            errors: [
              {
                status: '400',
                source: {pointer: '/shows'},
                title: 'Bad Request',
                detail: 'Page must be number.',
              },
            ],
          });
          return;
        } else {
          page = Number(req.query.page);
        }
      }
      try {
        const shows = await showService.listShows(page);
        if (shows.length) {
          res.status(200).json(shows);
        } else {
          res.status(404).send();
        }
      } catch (error) {
        logger.error(error);
        res.status(500).json({
          errors: [
            {
              status: '500',
              source: {pointer: '/shows'},
              title: 'Internal Server Error',
              detail: error,
            },
          ],
        });
      }
    });

    app.route('*').get((req: Request, res: Response) => {
      res.status(404).send();
    });
  }
}
