const swaggerDocument = {
  swagger: '2.0',
  info: {
    description: 'TvMaze Scraper and API for RTL',
    version: '1.0.0',
    title: 'rtl-tvmaze-scraper',
    contact: {
      email: 'gokhan.olgun@linkit.nl',
    },
  },
  paths: {
    '/shows': {
      get: {
        summary: 'get all shows',
        description: 'Returns all shows with casts information',
        parameters: [
          {
            in: 'query',
            name: 'page',
            type: 'number',
            description: 'Page number',
          },
        ],
        produces: ['application/json'],
        responses: {
          200: {
            description: 'All TV Shows',
            schema: {
              type: 'array',
              items: {
                $ref: '#/definitions/Show',
              },
            },
          },
        },
      },
    },
  },
  definitions: {
    Show: {
      type: 'object',
      required: ['id', 'name', 'casts'],
      properties: {
        id: {
          type: 'number',
          format: 'int32',
        },
        name: {
          type: 'string',
        },
        casts: {
          type: 'array',
          items: {
            $ref: '#/definitions/Cast',
          },
        },
      },
    },
    Cast: {
      type: 'object',
      properties: {
        id: {
          type: 'number',
        },
        name: {
          type: 'string',
        },
        birthday: {
          type: 'string',
        },
      },
    },
  },
  host: 'localhost:3000',
  basePath: '/',
  schemes: ['http'],
};

export = swaggerDocument;
