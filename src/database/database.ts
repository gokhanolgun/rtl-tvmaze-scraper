import * as dotenv from 'dotenv';
import * as mongoose from 'mongoose';
import logger from '../helpers/logger.helper';

dotenv.config();

export class Database {
  private db;
  private model;

  constructor(model) {
    this.model = model;
    this.db = mongoose;
    this.db.set('useCreateIndex', true);
    this.db
      .connect(
        process.env.MONGO_URL,
        {useNewUrlParser: true}
      )
      .catch(error => {
        logger.error(error);
        process.exit(1);
      });
  }

  public save(doc: Document) {
    const docModel = new this.model(doc);
    return docModel.save().catch(error => {
      logger.error(error);
    });
  }

  public update(query: object, data: object) {
    return this.model.updateMany(query, data).catch(error => {
      logger.error(error);
    });
  }

  public findOne(query: object, fields: any) {
    return this.model.findOne(query, fields).catch(error => {
      logger.error(error);
    });
  }

  public find(query: object, fields: any) {
    return this.model.find(query, fields).catch(error => {
      logger.error(error);
    });
  }

  public list(fields: any, page: number) {
    const limit = 10;
    if (page < 1) { page = 1; }
    const offset = (page - 1) * limit;
    return this.model.find({}, fields, {
      skip: offset,
      limit,
      sort: {
        id: 1,
      },
    });
  }
}
