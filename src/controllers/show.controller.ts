import {Database} from '../database/database';
import {ShowService} from '../services/show.service';

export class ShowController {
  private service;

  constructor(database: Database) {
    this.service = new ShowService(database);
  }

  public async listShows(page): Promise<object> {
    return this.service.listShows(page);
  }
}
