import * as mongoose from 'mongoose';

const ShowSchema = new mongoose.Schema({
  id: {type: Number, index: {unique: true}},
  name: String,
  updated: Number,
  casts: Array,
});

const ShowModel = mongoose.model('Show', ShowSchema);

export = ShowModel;
