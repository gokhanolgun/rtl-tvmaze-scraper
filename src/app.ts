import * as compression from 'compression';
import * as express from 'express';
import {Routes} from './routes';

class App {
  public app: express.Application;
  public router: Routes = new Routes();

  constructor() {
    this.app = express();
    this.app.use(compression());
    this.router.routes(this.app);
  }
}

export default new App().app;
