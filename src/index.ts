import * as dotenv from 'dotenv';
import * as cron from 'node-cron';
import app from './app';
import {Database} from './database/database';
import logger from './helpers/logger.helper';
import * as ShowModel from './models/show.model';
import {ScraperService} from './services/scraper.service';

dotenv.config();

const db = new Database(ShowModel);
const scraper = new ScraperService(db);
scraper.scrapeShows(); // start scraping tv shows' basic info from shows index

cron.schedule('0 * * * *', () => {
  logger.info('Running update task every hour');
  scraper.checkForUpdates(); // schedule updating shows' info and casts
});

app.listen(process.env.API_LISTEN_PORT, (err: string) => {
  if (err) {
    return logger.error(err);
  }

  return logger.info(`server is listening on ${process.env.API_LISTEN_PORT}`);
});
