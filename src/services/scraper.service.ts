import * as util from 'util';
import {Database} from '../database/database';
import logger from '../helpers/logger.helper';
import TvmazeService from './tvmaze.service';

export class ScraperService {
  private db;
  private isRunning;

  constructor(db: Database) {
    this.db = db;
  }

  public scrapeShows(page = 0): void {
    if (page === 0) {
      logger.info('Scraping shows index started');
    }
    this.isRunning = true;
    TvmazeService.getShowsIndex(page)
      .then(shows => {
        shows.forEach(show => {
          show.updated = 1; // To make it updatable on next cycle to get casts information
          this.db.save(show).catch(error => {
            if (error.code !== 11000) {
              // E11000 duplicate key error
              logger.error(error);
            }
          });
        });
        this.scrapeShows(++page);
      })
      .catch(error => {
        if (error === 404) {
          this.isRunning = false;
          this.checkForUpdates();
          logger.info('Scraping shows index completed');
        } else {
          logger.error(error);
        }
      });
  }

  public checkForUpdates(): void {
    if (this.isRunning) {
      return;
    }
    this.isRunning = true;
    logger.info('Fetching show updates');
    TvmazeService.getShowUpdates()
      .then(showUpdates => {
        logger.info('Show updates fetched');
        this.prepareShowsToBeUpdated(showUpdates);
      })
      .catch(error => {
        logger.error(error);
      });
  }

  private prepareShowsToBeUpdated(showUpdates): void {
    logger.info('Checking shows to be updated');
    let showsToUpdate = [];
    this.db.find({}, ['id', 'updated', '-_id']).then(shows => {
      shows.forEach(show => {
        if (showUpdates[show.id]) {
          if (showUpdates[show.id] !== show.updated) {
            showsToUpdate.push(show);
          }
          delete showUpdates[show.id];
        }
      });
      const showsToInsert = Object.keys(showUpdates).map(val => {
        return {id: Number(val), updated: 0};
      });
      logger.info(
        util.format(
          '%d show(s) will be updated and %d shows will be inserted',
          showsToUpdate.length,
          showsToInsert.length
        )
      );
      showsToUpdate = showsToUpdate.concat(showsToInsert);
      this.updateShowInfos(showsToUpdate);
    });
  }

  private async updateShowInfos(shows) {
    const show = shows.pop();
    TvmazeService.getShowInfoWithCasts(show.id).then(showResult => {
      const casts = [];
      showResult._embedded.cast.forEach(cast => {
        casts.push({
          id: cast.person.id,
          name: cast.person.name,
          birthday: cast.person.birthday,
        });
      });

      casts.sort((a, b) => {
        const aBirthday =
          a.birthday === null ? -Infinity : new Date(a.birthday).getTime();
        const bBirthday =
          b.birthday === null ? -Infinity : new Date(b.birthday).getTime();
        return bBirthday - aBirthday;
      });

      const newShow = {
        id: showResult.id,
        name: showResult.name,
        updated: showResult.updated,
        casts,
      };

      if (show.updated) {
        this.db
          .update({id: showResult.id}, newShow)
          .then((numAffected, err) => {
            if (err) {
              logger.error(err);
            }
          });
      } else {
        this.db.save(newShow);
      }

      if (shows.length) {
        this.updateShowInfos(shows);
      } else {
        this.isRunning = false;
        logger.info('Update completed');
      }
    });
  }
}
