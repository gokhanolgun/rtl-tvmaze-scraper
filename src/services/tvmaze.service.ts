import * as request from 'requestretry';
import * as util from 'util';
import logger from '../helpers/logger.helper';
import {Icast, Ishow, Iupdates} from '../interfaces/tvmaze.interfaces';

const retryStrategy = (err, response, body) => {
  if (err || (response && response.statusCode === 429)) {
    if (err) {
      logger.info(err);
    }
    if (response && response.statusCode === 429) {
      logger.info('Waiting for rate limit');
    }
    return true;
  }
};

class TvmazeService {
  public static async getShowsIndex(page: number): Promise<Ishow[]> {
    return this.apiQuery<Ishow[]>(
      util.format(process.env.TVMAZE_API_SHOWS_INDEX_URL, page)
    );
  }

  public static async getShowUpdates(): Promise<Iupdates> {
    return this.apiQuery<Iupdates>(process.env.TVMAZE_API_UPDATES_URL);
  }

  public static async getShowInfoWithCasts(showId: number): Promise<Ishow> {
    return this.apiQuery<Ishow>(
      util.format(process.env.TVMAZE_API_SHOW_INFO_W_CASTS_URL, showId)
    );
  }

  public static async getShowCasts(showId: number): Promise<Icast[]> {
    return this.apiQuery<Icast[]>(
      util.format(process.env.TVMAZE_API_CASTS_URL, showId)
    );
  }

  private static apiQuery<T>(url: string): Promise<T> {
    return new Promise((resolve, reject) => {
      request.get(
        {
          url,
          json: true,
          maxAttempts: 100,
          retryDelay: 5000,
          retryStrategy,
        },
        (err, response) => {
          if (err) {
            logger.error('No more retry');
            return reject(err);
          }
          if (response !== undefined) {
            if (response.statusCode === 200) {
              resolve(response.body);
            } else {
              reject(response.statusCode);
            }
          }
        }
      );
    });
  }
}

export default TvmazeService;
