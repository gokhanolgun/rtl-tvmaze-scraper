import {Database} from '../database/database';
import TvmazeService from './tvmaze.service';

export class ShowService {
  private collectionName = 'cities';
  private db;

  constructor(database: Database) {
    this.db = database;
  }

  public async listShows(page = 1) {
    const shows = await this.db.list(['id', 'name', 'casts', '-_id'], page);
    for (const i in shows) {
      if (shows[i]) {
        const show = shows[i];
        if (show.casts.length === 0) {
          const casts = await this.updateCasts(show.id);
          show.casts = casts;
        }
      }
    }
    return shows;
  }

  private async updateCasts(showId) {
    return TvmazeService.getShowCasts(showId).then(rawCasts => {
      const casts = [];
      rawCasts.forEach(cast => {
        casts.push({
          id: cast.person.id,
          name: cast.person.name,
          birthday: cast.person.birthday,
        });
      });
      casts.sort((a, b) => {
        const aBirthday =
          a.birthday === null ? -Infinity : new Date(a.birthday).getTime();
        const bBirthday =
          b.birthday === null ? -Infinity : new Date(b.birthday).getTime();
        return bBirthday - aBirthday;
      });
      this.db.update({id: showId}, {casts});
      return casts;
    });
  }
}
